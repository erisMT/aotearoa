# Aotearoa REDO

A Minetest mod that complements the default map biomes with Aotearoa, land of the long white cloud, New Zealand.

Contains a large number of realistic native flora, biomes, and more. A land of dark towering forests, windswept mountains, and more.

Original mod created by Dokimi (provided all floral and geological knowledge, completed the original base for the mod by themselves).

Contributed to by Josselin2 (various improvements, fixes) and erisMT (maintainer of this fork).

![Screenshot](screenshot.png)

# Instructions:

Designed for use with mg_valleys.
(Will work with other default mapgens but may not display well. )

# License:

Code is licensed under GNU LGPLv2+ and GNU LGPLv3+.

Textures are licensed under CC BY-SA 3.0 Unported.

See LICENSE.txt for details.

# Credit:

NZ birdsongs adapted from New Zealand Department of Conservation. CC BY 4.0
https://www.doc.govt.nz/nature/native-animals/birds/bird-songs-and-calls/
(bellbirds, tuis, kokako, parakeet, si_kaka, tuis2, tuis3, tuis4, silvereye, fantail1, grey_warbler, ni_robin, si_robin, cicada_kereru, whitehead, kea, takahe, morepork, male_kiwi, female_kiwi, kakapo, shearwater, yellow_eyed_penguin)

boiling_mud sound adapted from Bubbling Cauldron, Mike Koenig , http://soundbible.com/51-Bubbling-Cauldron.html

mud footstep sounds (http://www.freesound.org/people/dobroide/sounds/16771) copyright (C) 2006 by dobroide, [CC by 3.0]

te_kore_whakapapa adapted from https://teara.govt.nz/en/speech/30768/te-kore-whakapapa  Creative Commons Attribution-NonCommercial 3.0 New Zealand Licence. See below for text and translation.

# Te Kore Whakapapa:

The Geneaology of Creation:

I te timatanga Te Kore.
Te Kore.
Te Kore-te-whiwhia.
Te Kore-te-rawea.
Te Kore-i-ai.
Te Kore-te-wiwia.

Te Kore Te Po.
Te Po-nui.
Te Po-roa.
Te Po-uriuri.
Te Po-kerekere.
Te Po-tiwhatiwha.
Te Po-te-kitea.
Te Po-tangotango.
Te Po-whawha.
Te Po-namunamu-ki-taiao.
Te Po-tahuri-atu.
Te Po-tahuri-mai-ki-taiao.

Te Whai-ao.
Te Ao-marama.
Tihei mauri-ora.

-----

In the beginning was the Void.
The Void.
The Void in which nothing is possessed.
The Void in which nothing is felt.
The Void with nothing in union.
The Void without boundaries.

From the void the Night.
The great Night.
The long Night.
The deep Night.
The intense Night.
The dark Night.
The Night in which nothing is seen.
The intensely dark Night.
The Night of feeling.
The Night of seeking the passage to the world.
The Night of restless turning.
The Night of turning towards the revealed world.

The glimmer of dawn.
The bright light of day.
There is life.
