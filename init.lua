-- Definitions made by this mod that other mods can use too.
aotearoa = {}
local path = minetest.get_modpath("aotearoa")

local default_loaded = minetest.get_modpath("default")
local zero_loaded = minetest.get_modpath("zr_wood")

-- Load settings.
aotearoa.settings = {
	clear_biomes = minetest.settings:get_bool("aotearoa.clear_biomes") or false,
	clear_decos  = minetest.settings:get_bool("aotearoa.clear_decos") or false,
	clear_ores   = minetest.settings:get_bool("aotearoa.clear_ores") or false,
}

if aotearoa.settings.clear_biomes then
	minetest.clear_registered_biomes()
end
if aotearoa.settings.clear_decos then
	minetest.clear_registered_decorations()
end
if aotearoa.settings.clear_ores then
	minetest.clear_registered_ores()
end


-- Load files.
dofile(path .. "/nodes.lua")
dofile(path .. "/schematics.lua")
dofile(path .. "/ores.lua")
dofile(path .. "/mapgen.lua")

if default_loaded then
	dofile(path .. "/schematic_decos_default.lua")
	dofile(path .. "/simple_decos_default.lua")
elseif zero_loaded then
	dofile(path .. "/schematic_decos_zero.lua")
	dofile(path .. "/simple_decos_zero.lua")
end
dofile(path .. "/spawn_aquatic.lua")
dofile(path .. "/trees.lua")
dofile(path .. "/crafting.lua")

-- Green shovel
if not minetest.get_modpath("ethereal") then
	dofile(path .. "/green_shovel.lua")
end

-- Use immersive sounds, if possible.
if minetest.get_modpath("ambience") then
	dofile(path .. "/ambience.lua")
end

minetest.log("action", "[Aotearoa] Successfully loaded.")


----------------------
-- Intro world creation (the geneaology of creation).
-- Seeds to make up for removing default/zero sources.
if default_loaded then
	minetest.register_on_newplayer(function(player)
		local you = player:get_player_name()
		local inventory = player:get_inventory()
		inventory:add_item("main", "farming:seed_wheat 2")
		inventory:add_item("main", "farming:seed_cotton 2")
		inventory:add_item("main", "default:axe_stone")
		minetest.sound_play("te_kore_whakapapa", {to_player = you, gain = 2})
	end)
elseif zero_loaded then
	minetest.register_on_newplayer(function(player)
		local you = player:get_player_name()
		local inventory = player:get_inventory()
		inventory:add_item("main", "zr_farm:seed_wheat 2")
		inventory:add_item("main", "zr_farm:seed_cotton 2")
		inventory:add_item("main", "zr_stone:axe")
		minetest.sound_play("te_kore_whakapapa", {to_player = you, gain = 2})
	end)
end
